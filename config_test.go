package conf

import (
	"testing"
)

func TestGetFirst(t *testing.T) {
	Init("test")
	SetConfig([]byte(`{"app": "test", "key": {"subkey": "val"}}`), "01-test")
	val, ok := FirstString([]string{"nil"}, []string{"key", "subkey"}, []string{"app"})
	if !ok {
		t.Error("Value not found")
	}
	if val != "val" {
		t.Error("Incorrect value")
	}
}