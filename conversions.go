package conf

import (
	"github.com/buger/jsonparser"
	"log"
	"strconv"
	"sync"
)

func String(keys ...string) (string, bool) {
	val, ok := Get(keys...)
	if ok {
		return string(val), true
	}
	return "", false
}

// Итерировать список ключей, вернуть первое значение найденное в конфиге
// Функции с префиксом First- придуманы для удобства имплементации дефолтных значений
func FirstString(keysList ...[]string) (string, bool) {
	for _, key := range keysList {
		if value, ok := String(key...); ok {
			return value, true
		}
	}
	return "", false
}

var stringArrayCache map[string][]string
var stringArrayMutex = sync.Mutex{}

func StringArray(keys ...string) ([]string, bool) {
	hash := getKeysHash(keys...)

	stringArrayMutex.Lock()
	vals, ok := stringArrayCache[hash]
	stringArrayMutex.Unlock()
	if ok {
		return vals, true
	}

	val, ok := Get(keys...)
	if ok {
		vals := []string{}
		jsonparser.ArrayEach(val, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			vals = append(vals, string(value))
		})

		stringArrayMutex.Lock()
		stringArrayCache[hash] = vals
		stringArrayMutex.Unlock()

		return vals, true
	}
	return []string{}, false
}

func FirstStringArray(keysList ...[]string) ([]string, bool) {
	for _, key := range keysList {
		if value, ok := StringArray(key...); ok {
			return value, true
		}
	}
	return []string{}, false
}

func StringMap(keys ...string) (map[string]string, bool) {
	// todo: cache?
	vals := map[string]string{}
	val, ok := Get(keys...)
	if !ok {
		return vals, false
	}

	jsonparser.ObjectEach(val, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		vals[string(key)] = string(value)
		return nil
	})

	return vals, true
}

func Uint64(keys ...string) (uint64, bool) {
	str, ok := String(keys...)
	if !ok {
		return 0, false
	}
	val, err := strconv.ParseUint(str, 10, 64)
	if err != nil {
		log.Println("Error: Value", keys, "is not int")
		return 0, false
	}
	return val, true
}

func FirstUint64(keysList ...[]string) (uint64, bool) {
	for _, key := range keysList {
		if value, ok := Uint64(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Uint32(keys ...string) (uint32, bool) {
	val, ok := Uint64(keys...)
	if ok {
		return uint32(val), true
	}
	return 0, false
}

func FirstUint32(keysList ...[]string) (uint32, bool) {
	for _, key := range keysList {
		if value, ok := Uint32(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Int64(keys ...string) (int64, bool) {
	str, ok := String(keys...)
	if !ok {
		return 0, false
	}
	val, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		log.Println("Error: Value", keys, "is not int")
		return 0, false
	}
	return val, true
}

func FirstInt64(keysList ...[]string) (int64, bool) {
	for _, key := range keysList {
		if value, ok := Int64(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Int32(keys ...string) (int32, bool) {
	val, ok := Int64(keys...)
	if ok {
		return int32(val), true
	}
	return 0, false
}

func FirstInt32(keysList ...[]string) (int32, bool) {
	for _, key := range keysList {
		if value, ok := Int32(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Int8(keys ...string) (int8, bool) {
	val, ok := Int64(keys...)
	if ok {
		return int8(val), true
	}
	return 0, false
}

func FirstInt8(keysList ...[]string) (int8, bool) {
	for _, key := range keysList {
		if value, ok := Int8(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Int(keys ...string) (int, bool) {
	val, ok := Int64(keys...)
	if ok {
		return int(val), true
	}
	return 0, false
}

func FirstInt(keysList ...[]string) (int, bool) {
	for _, key := range keysList {
		if value, ok := Int(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Float64(keys ...string) (float64, bool) {
	str, ok := String(keys...)
	if !ok {
		return 0, false
	}
	val, err := strconv.ParseFloat(str, 64)
	if err != nil {
		log.Println("Error: Value", keys, "is not float")
		return 0, false
	}
	return val, true
}

func FirstFloat64(keysList ...[]string) (float64, bool) {
	for _, key := range keysList {
		if value, ok := Float64(key...); ok {
			return value, true
		}
	}
	return 0, false
}

func Float32(keys ...string) (float32, bool) {
	val, ok := Float64(keys...)
	if ok {
		return float32(val), true
	}
	return 0, false
}

func FirstFloat32(keysList ...[]string) (float32, bool) {
	for _, key := range keysList {
		if value, ok := Float32(key...); ok {
			return value, true
		}
	}
	return 0, false
}
