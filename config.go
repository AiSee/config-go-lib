package conf

import (
	"errors"
	"github.com/buger/jsonparser"
	"github.com/dgryski/go-farm"
	"github.com/orcaman/concurrent-map"
	"log"
	"os"
	"strconv"
	"strings"
	"io/ioutil"
	"sort"
)

var configs cmap.ConcurrentMap // Список конфигов
var configNames []string // Имена конфигов из списка, требуется для сортировки списка по ключу
var cache cmap.ConcurrentMap // Кеш значений конфига
var appName string // Имя приложения

func getKeysHash(keys ...string) string {
	return strconv.FormatUint(farm.Hash64([]byte(strings.Join(keys, "|"))), 36)
}

func Get(keys ...string) ([]byte, bool) {
	// jsonparser может упасть и тут, к сожалению
	defer func() {
		if r := recover(); r != nil {
			log.Println("Error: Recovered", r)
		}
	}()
	hash := getKeysHash(keys...)
	// Проверим сначала кеш
	if val, ok := cache.Get(hash); ok {
		return val.([]byte), true
	}
	// Перебираем имена конфигов (они отсортированы по возрастанию)
	for _, name := range configNames {
		// Получаем сам конфиг из кеша
		if data, ok := configs.Get(name); ok {
			// Это должен быть срез байт
			if config, ok := data.([]byte); ok {
				// Ищем в конфиге нужное значение
				val, _, _, err := jsonparser.Get(config, keys...)
				if err == nil {
					// Запишем в кеш и вернём
					cache.Set(hash, val)
					return val, true
				}
			}
		}
		// Если не смогли найти в этом конфиге, ищем в следующем
	}
	// А вот если совсем ничего не смогли найти, то это очень плохо
	// log.Println("Error: Value", keys, "not found!")
	return []byte{}, false
}

// Каждый конфиг должен иметь поле "app": appName для проверки json, и чтобы нельзя было случайно подсунуть другой json
func CheckJson(data []byte, appName string) bool {
	// Библиотека может упасть, но надеюсь, все падения можно поймать на таком типе проверок
	defer func() {
		if r := recover(); r != nil {
			log.Println("Error: Recovered", r)
		}
	}()
	app, err := jsonparser.GetString(data, "app")
	if err != nil || app != appName {
		return false
	}
	return true
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Конфиги будут применяться в порядке возрастания имени ключа. Ex: 01-file, 02-add, 99-default
func SetConfig(config []byte, name string) error {
	if !CheckJson(config, appName) {
		return errors.New("Config: " + name + " - json check failed or field \"app\": \"" + appName + "\" not found")
	}
	// Добавляем конфиг
	configs.Set(name, config)
	// Список имён должен оставаться уникальным
	if !stringInSlice(name, configNames) {
		configNames = append(configNames, name)
	}
	// Сортируем список, чтобы применять конфиги в правильном порядке
	sort.Strings(configNames)
	// Сбрасываем кеш
	cache = cmap.New()
	stringArrayMutex.Lock()
	stringArrayCache = make(map[string][]string)
	stringArrayMutex.Unlock()
	return nil
}

func SetConfigFromFile(fileName, name string) error {
	if !fileExists(fileName) {
		return errors.New("Config file: " + fileName + " - doesn't exist")
	}
	config, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}
	return SetConfig(config, name)
}

func Init(applicationName string) {
	cache = cmap.New()
	configs = cmap.New()
	stringArrayCache = make(map[string][]string)
	appName = applicationName
}
